package ribomation.clean_code.functions;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Validator {
    private static final SimpleDateFormat dateFmt = new SimpleDateFormat("yyyyMMdd");
    private Calendar today;

    public Validator(Calendar today) { this.today = today; }

    String normalize(String pnr) {
        if (pnr.length() == 12) return pnr;
        if (pnr.length() == 13) return pnr.replaceFirst("[+-]", "");
        if (pnr.contains("+")) return "19" + pnr.replaceFirst("[+]", "");

        pnr = pnr.replaceFirst("[-]", "");
        int birthYear   = 2000 + Integer.parseInt(pnr.substring(0, 2));
        int currentYear = today.get(Calendar.YEAR);
        if (currentYear < birthYear) return "19" + pnr;

        String yearPrefix = ((currentYear != birthYear) || isYoung(pnr) ? "20" : "19");
        return yearPrefix + pnr;
    }

    private boolean isYoung(String pnr) {
        String todayStr   = dateFmt.format(today.getTime());
        String birthStr   = "20" + pnr.substring(0, 6);
        return birthStr.compareTo(todayStr) > 0;
    }

}
