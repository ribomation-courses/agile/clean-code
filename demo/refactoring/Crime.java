package refactoring;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Crime {
    SimpleDateFormat iso8601 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String title;
    String description;
    String place;
    Date   date;

    public Crime(Node node) {
        this((Element) node);
    }

    public Crime(Element e)  {
        this(textOf(e, "title"), textOf(e, "text"), textOf(e, "place"), textOf(e, "date"));
    }

    public Crime(String title, String description, String place, String date)  {
        this.title = title;
        this.description = description;
        this.place = place;
        try {
            this.date = iso8601.parse(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException("malformed date", e);
        }
    }

    private static String textOf(Element e, String tag) {
        NodeList nodes = e.getElementsByTagName(tag);
        if (nodes.getLength() > 0) {
            return nodes.item(0).getTextContent();
        }
        return "";
    }

    public boolean contains(String crimeType) {
        return title.toLowerCase().contains(crimeType) || description.toLowerCase().contains(crimeType);
    }
}
