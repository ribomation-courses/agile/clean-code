package ribomation.clean_code.testing

import spock.lang.Specification

class SumSpec extends Specification {
    def 'simple values should pass'() {
        given: 'a Sum object'
        def sum = new Sum()

        expect:
        sum.compute(arg) == result

        where:
        arg || result
        2    || 3
        10   || 55
        100  || 5050
        1000 || 500500
    }

    def 'sum(1..10000) should pass'() {
        given: 'a Sum object'
        def sum = new Sum()

        when: 'invoking compute'
        def result = sum.compute(10_000)

        then: 'we should have a large value'
        result == 50_005_000
    }

    def 'sum(1..1000000) should pass'() {
        given: 'a Sum object'
        def sum = new Sum()

        when: 'invoking compute'
        def result = sum.compute(1_000_000)

        then: 'we should have a very large value'
        result == 500000500000
    }
}
