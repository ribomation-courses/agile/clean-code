package ribomation.clean_code.testing;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * Caches argument and result pairs.
 */
public class ArgumentAndResultCache {
    private Map<Integer, BigInteger> cache = new HashMap<>();

    public boolean has(int arg) {
        return cache.containsKey(arg);
    }

    public BigInteger get(int arg) {
        return cache.get(arg);
    }

    public void put(int arg, BigInteger result) {
        cache.put(arg, result);
    }

    public void clear() {
        cache.clear();
    }
}
