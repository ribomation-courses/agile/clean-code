package ribomation.clean_code.errors;

import org.junit.Before;
import org.junit.Test;
import org.omg.PortableInterceptor.Interceptor;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CircularBufferTest {
    private final int N = 3;
    private CircularBuffer<Integer> target;

    @Before
    public void setUp() throws Exception {
        target = new CircularBuffer<>(N);
    }

    @Test
    public void empty() throws Exception {
        assertThat(target.empty(), is(true));
        assertThat(target.full(), is(false));
    }

    @Test
    public void full() throws Exception {
        assertThat(target.empty(), is(true));
        for (int k = 0; k < N; ++k) {
            target.put(k);
            assertThat(target.empty(), is(false));
        }
        assertThat(target.empty(), is(false));
        assertThat(target.full(), is(true));
    }

    @Test
    public void size() throws Exception {
        assertThat(target.size(), is(0));
        for (int k = 0; k < N; ++k) {
            target.put(k);
            assertThat(target.size(), is(k+1));
        }
        assertThat(target.size(), is(N));
    }

    @Test
    public void capacity() throws Exception {
        assertThat(target.capacity(), is(N));
    }

    @Test
    public void put() throws Exception {
        assertThat(target.put(null), is(-2));
        for (int k = 0; k < N; ++k) target.put(k);
        assertThat(target.put(42), is(-1));
    }

    @Test
    public void get() throws Exception {
        assertThat(target.get(new Integer[1]), is(-1));

        for (int k = 0; k < N; ++k) target.put(k+1);

        int k = 1;
        while (!target.empty()) {
            Integer[] r = new Integer[1];
            int rc = target.get(r);
            assertThat(rc, is(0));
            assertThat(r[0], is(k++));
        }

        assertThat(target.get(new Integer[10]), is(-2));
        assertThat(target.get(null), is(-3));
    }

}