package com.ribomation.clean_code;

/**
 * Responsible for constructing and sending an email message.
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2016-08-18
 */
public class EmailBuilder {
    private Message message;

    static EmailBuilder create() {
        return new EmailBuilder();
    }

    private EmailBuilder() {
        message = new Message();
        message.setFrom("no-reply@foobar.com");
        message.setTo("devops@foobar.com");
        message.setSubject("Just a test");
    }

    public EmailBuilder from(String value) {
        message.setFrom(value);
        return this;
    }

    public EmailBuilder to(String value) {
        message.setTo(value);
        return this;
    }

    public EmailBuilder cc(String value) {
        message.setCc(value);
        return this;
    }

    public EmailBuilder subject(String value) {
        message.setSubject(value);
        return this;
    }

    public EmailBuilder appendToBody(String value) {
        String body = message.getBody();
        if (body == null) {
            message.setBody(value);
        } else {
            message.setBody(String.format("%s%n%s", body, value));
        }
        return this;
    }

    public Message build() {
        return message;
    }

}
