package com.ribomation.clean_code;

/**
 * Knows how to send an email message
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2016-08-18
 */
public interface EmailTransport {
    void send(Message message);
}
