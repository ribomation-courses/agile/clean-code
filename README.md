Clean Code, 2 days
====

Welcome to this course.
The syllabus can be find at
[agile/clean-code](https://www.ribomation.se/courses/agile/clean-code.html)

Here you will find
* Installation instructions
* Start code to the exercises
* Solutions to the exercises


Installation Instructions
====

You need to have the following installed
* [GIT Client](https://git-scm.com/downloads)
* [Java JDK](https://adoptopenjdk.net/index.html?variant=openjdk13&jvmVariant=hotspot)
* A decent Java IDE, such as 
    - [Jetbrains Intellij IDEA](https://www.jetbrains.com/idea/download/#section=windows)
    - [MS Visual Code](https://code.visualstudio.com/)

Course GIT Repo
====
Get the course repo initially by a `git clone` operation

    git clone https://gitlab.com/ribomation-courses/agile/clean-code.git
    cd clean-code

Get any updates by a `git pull` operation

    git pull


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>



