package ribomation.clean_code.errors;

public class Consumer<T> extends Thread {
    private BoundedQueue<T> in;

    public Consumer(BoundedQueue<T> in) {
        this.in = in;
    }

    @Override
    public void run() {
        while (!in.isDone()) {
            T x = in.get();
            System.out.printf("[consumer] %s%n", x);
        }
    }
}
