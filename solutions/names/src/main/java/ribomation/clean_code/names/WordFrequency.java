package ribomation.clean_code.names;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WordFrequency {
    public static void main(String[] args) throws Exception {
        new WordFrequency().run(input(args));
    }

    static InputStream input(String[] filenames) throws FileNotFoundException {
        if (filenames.length == 1) {
            return new FileInputStream(filenames[0]);
        } else {
            return WordFrequency.class.getResourceAsStream("/ttm.txt");
        }
    }

    void run(InputStream input) throws IOException {
        int minWordSize = 4;
        int maxNumberOfWords = 20;
        Map<String, Long> wordFrequencies = loadWordsAndTheirFrequenciesAlt(input, minWordSize);
        printFrequencyTable(wordFrequencies, maxNumberOfWords);
    }

    Map<String, Long> loadWordsAndTheirFrequencies(InputStream is, int minWordSize) throws IOException {
        Map<String, Long> wordFrequencies = new HashMap<>();
        BufferedReader       input = new BufferedReader(new InputStreamReader(is));
        for (String line = input.readLine(); line != null; line = input.readLine()) {
            for (String word : line.split("[^a-zA-Z]+")) {
                if (word.length() < minWordSize) continue;
                word = word.toLowerCase();
                wordFrequencies.put(word, wordFrequencies.getOrDefault(word, 0L) + 1);
            }
        }
        return wordFrequencies;
    }

    void printFrequencyTable(Map<String, Long> wordFrequencies, int maxNumberOfWords) {
        List<Map.Entry<String, Long>> wordFreqTuples = new ArrayList<>(wordFrequencies.entrySet());

        wordFreqTuples.sort((left, right) -> Long.compare(right.getValue(), left.getValue()));

        int wordCount = 0;
        for (Map.Entry<String, Long> e : wordFreqTuples) {
            System.out.printf("%12s: %d%n", e.getKey(), e.getValue());
            if (++wordCount >= maxNumberOfWords) return;
        }
    }


    Map<String, Long> loadWordsAndTheirFrequenciesAlt(InputStream is, int minWordSize) throws IOException {
       return new BufferedReader(new InputStreamReader(is))
        .lines()
        .flatMap(line -> Pattern.compile("[^a-zA-Z]+").splitAsStream(line))
        .filter(word -> word.length() >= minWordSize)
        .map(String::toLowerCase)
        .collect(Collectors.groupingBy(word -> word, Collectors.counting()))
        ;
    }

}
