package com.ribomation.clean_code;

/**
 * Simple test program
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2016-08-18
 */
public class App {
    public static void main(String[] args) {
        App app = new App();
        app.config();
        app.run(app.transport);
    }

    public void config() {
        transport = new DummyEmailTransport();
    }

    private EmailTransport transport;

    public void run(EmailTransport transport) {
        Message message = EmailBuilder.create()
                .from("daemon@ribomation.se")
                .to("jens.riboe@ribomation.se")
                .cc("logger@ribomation.se")
                .subject("Spooky behaviour")
                .appendToBody("Last night there was some fishy actions going on.")
                .appendToBody("The servers kept failing over.")
                .appendToBody("Please, fix that ASAP.")
                .appendToBody("Cheers /sys-elf")
                .build();
        transport.send(message);
    }
}
