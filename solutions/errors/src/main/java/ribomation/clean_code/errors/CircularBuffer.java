package ribomation.clean_code.errors;

public class CircularBuffer<T> {
    private T[] buffer;
    private int putIdx = 0, getIdx = 0, size = 0;

    public static class EmptyBuffer extends RuntimeException { }
    public static class FullBuffer extends RuntimeException { }

    @SuppressWarnings("unchecked")
    public CircularBuffer(int capacity) {
        assert capacity > 0 : "capacity must be a positive value";
        buffer = (T[]) new Object[capacity];
    }

    public int capacity() {
        return buffer.length;
    }

    public int size() {
        return this.size;
    }

    public boolean empty() {
        return size() == 0;
    }

    public boolean full() {
        return size() >= capacity();
    }

    public void put(T x) {
        assert x != null : "payload must not be null";
        if (full()) throw new FullBuffer();

        buffer[putIdx] = x;
        putIdx = (putIdx + 1) % capacity();
        ++size;
    }

    public T get() {
        if (empty()) throw new EmptyBuffer();

        T x = buffer[getIdx];
        getIdx = (getIdx + 1) % capacity();
        --size;

        return x;
    }

}
