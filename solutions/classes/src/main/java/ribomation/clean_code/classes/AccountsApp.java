package ribomation.clean_code.classes;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class AccountsApp {
    public static void main(String[] args) throws Exception {
        AccountsApp app = new AccountsApp();
        app.run(args);
    }

    void run(String[] args) throws Exception {
        String filename = "./src/main/resources/accounts.csv";
        if (args.length > 0) {
            filename = args[0];
        }

        List<String[]> data  = loadAccounts(filename);
        AccountsModel  model = new AccountsModel(data);
        MetaData       metaData = new MetaData();
        MainWindow     gui   = new MainWindow(model, metaData, filename);
        gui.showGui();
    }

    List<String[]> loadAccounts(String filename) throws Exception {
        if (!Paths.get(filename).toFile().canRead()) {
            throw new IllegalArgumentException("Cannot find file: " + filename);
        }

        return Files.lines(Paths.get(filename))
                .map((line) -> line.split((",")))
                .collect(Collectors.toList())
                ;
    }

}
