package ribomation.clean_code.testing;

import java.math.BigInteger;

/**
 * Computes SUM(1..n)
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2016-04-28
 */
public class Sum extends CachingUnaryFunction {
    private static final BigInteger TWO = BigInteger.valueOf(2);

    /**
     * Uses the formula: SUM(1..n) = n(n+1)/2, n > 1
     *
     * @param n
     * @return n(n+1)/2
     */
    @Override
    protected BigInteger computeResult(int n) {
        return BigInteger.valueOf(n).multiply(BigInteger.valueOf(n + 1)).divide(TWO);
    }
}
