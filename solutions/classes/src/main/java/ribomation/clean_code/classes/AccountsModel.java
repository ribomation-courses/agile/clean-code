package ribomation.clean_code.classes;

import javax.swing.table.AbstractTableModel;
import java.util.List;
import java.util.stream.Stream;

/**
 * Swing table-model for the parsed accounts data
 */
public class AccountsModel extends AbstractTableModel {
    private final List<String[]> data;

    public AccountsModel(List<String[]> data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size() - 1;
    }

    @Override
    public int getColumnCount() {
        return data.get(0).length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data.get(rowIndex + 1)[columnIndex];
    }

    @Override
    public String getColumnName(int column) {
        return data.get(0)[column].toUpperCase();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    private Stream<String> selectDataColumn(int colIndex) {
        return data.stream().skip(1).map((d) -> d[colIndex]);
    }

    double getAmountSum() {
        return selectDataColumn(1).mapToDouble(Double::parseDouble).sum();
    }

    long getCurrencyCount() {
        return selectDataColumn(1).distinct().count();
    }

    long getCountryCount() {
        return selectDataColumn(5).distinct().count();
    }

}
