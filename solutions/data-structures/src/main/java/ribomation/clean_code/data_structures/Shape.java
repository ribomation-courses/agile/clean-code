package ribomation.clean_code.data_structures;

public abstract class Shape {
    protected Point position;

    public Shape(Point position) {
        this.position = position;
    }

    public abstract double area();
    public abstract double perimeter();
    protected abstract String dimension();

    @Override
    public String toString() {
        String type = getClass().getSimpleName().toUpperCase().substring(0, 4);
        return String.format("%s{%2d,%2d | %s} area=%.0f, perimeter=%.0f",
                type,
                position.x, position.y,
                dimension(),
                area(),
                perimeter()
        );
    }

}


