package ribomation.clean_code.data_structures;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class Shapes {

    public static void main(String[] args) throws IOException {
        Shapes app  = new Shapes();
        String file = app.init(args);
        app.run(file);
    }

    String init(String[] args) {
        if (args.length > 0) {
            return args[0];
        }
        return "./src/main/resources/shapes.csv";
    }

    void run(String filename) throws IOException {
        List<Shape> shapes = Files.lines(Paths.get(filename))
                .skip(1)
                .map((line) -> line.split(","))
                .map(this::create)
                .collect(Collectors.toList());

        int N = 15;
        String sampleShapes = shapes.stream()
                .limit(N)
                .map(this::toString)
                .collect(Collectors.joining("\n"));
        System.out.printf("%d first shapes:%n%s%n", N, sampleShapes);

        double totalArea = shapes.stream()
                .map(this::area)
                .reduce(0D, Double::sum);
        System.out.printf(Locale.ENGLISH, "total area = %.3f%n", totalArea);

        double totalPerimeter = shapes.stream()
                .map(this::perimeter)
                .reduce(0D, Double::sum);
        System.out.printf(Locale.ENGLISH, "total perimeter = %.3f%n", totalPerimeter);
    }


    static class Shape {
        Type type;
        int  x, y, w, h;
    }

    enum Type {
        rectangle, circle, triangle, square
    }

    Shape create(String[] v) {
        Shape shape = new Shape();
        int   ix    = 0;
        shape.type = typeOf(v[ix++]);
        shape.x = Integer.parseInt(v[ix++]);
        shape.y = Integer.parseInt(v[ix++]);
        shape.w = Integer.parseInt(v[ix++]);
        shape.h = Integer.parseInt(v[ix++]);
        return shape;
    }

    Type typeOf(String t) {
        switch (t) {
            case "rect":
                return Type.rectangle;
            case "circ":
                return Type.circle;
            case "tria":
                return Type.triangle;
            case "squa":
                return Type.square;
        }
        throw new IllegalArgumentException("no such shape type: " + t);
    }

    double area(Shape s) {
        switch (s.type) {
            case rectangle:
                return s.w * s.h;
            case circle:
                double r = s.w / 2D;
                return Math.PI * r * r;
            case triangle:
                return s.w * s.h / 2;
            case square:
                return s.w * s.w;
        }
        return 0;
    }

    double perimeter(Shape s) {
        switch (s.type) {
            case rectangle:
                return 2 * (s.w + s.h);
            case circle:
                double r = s.w / 2D;
                return Math.PI * 2 * r;
            case triangle:
                double W = s.w / 2D;
                double S = Math.sqrt(W * W + s.h * s.h);
                return 2 * (W + S);
            case square:
                return 4 * s.w;
        }
        return 0;
    }

    String toString(Shape s) {
        return String.format("%s{%d,%d | %d,%d} A=%.0f, P=%.0f",
                s.type.name().toUpperCase(), s.x, s.y, s.w, s.h, area(s), perimeter(s)
        );
    }

}
