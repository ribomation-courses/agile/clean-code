package ribomation.clean_code.testing;

import java.math.BigInteger;

/**
 * Represents a computing function: <i>f(x)</i>
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2016-04-28
 */
public interface UnaryFunction {

    /**
     * Computes the result given the argument
     * @param arg   argument
     * @return its result
     */
    BigInteger compute(int arg);

}
